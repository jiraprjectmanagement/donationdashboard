
//   Home Page JS

$(function () {

$('.total-pldge-amount').easyPieChart({
      easing: false,
      barColor: '#000',
      rackColor: false,
      scaleColor: false,
      scaleLength: 3,
      lineCap: 'circle',
      lineWidth: '3',
      size: 77,
      trackColor: '#ccc'
  });


$('.raised-amount-counter').easyPieChart({
   easing: false,
    barColor: '#285993',
    rackColor: false,
    scaleColor: false,
    scaleLength: 3,
    lineCap: 'circle',
    lineWidth: '3',
    size: 93,
    trackColor: '#ccc'
});







  // Get titles from the DOM
var titleMain  = $("#commentanimatedHeading");
var titleSubs  = titleMain.find("slick-active");

if (titleMain.length) {

titleMain.slick({
  autoplay: false,
  arrows: false,
  dots: false,
  slidesToShow: 2,
  // centerPadding: "10px",
  draggable: false,
  infinite: true,
  pauseOnHover: false,
  swipe: false,
  touchMove: false,
  vertical: true,
  speed: 1000,
  autoplaySpeed: 2000,
  useTransform: true,
  cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
  adaptiveHeight: true,
  responsive: [
    {
      breakpoint: 1920,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 1605,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 1367,
      settings: {
        slidesToShow: 2,
      }
    }
  ]
});

// On init
$(".slick-dupe").each(function(index, el) {
  $("#commentanimatedHeading").slick('slickAdd', "<div>" + el.innerHTML + "</div>");
});

// Manually refresh positioning of slick
titleMain.slick('slickPlay');
};
  



// $('#animatedHeading').slick({
//    vertical:true,
//    verticalSwiping:true,
//    slidesToShow: 5,
//    slidesToScroll: 1,
//    autoplay: true,
//    autoplaySpeed: 0,
//    speed:5000,
//    cssEase: 'linear',
//    infinite: true,
//    arrows:false,
//    slidesToShow: 4,
//    slidesToScroll: 1,
//    touchMove:true,
//    swipeToSlide:true,
//    swipe:true,
//    responsive: [
//      {
//        breakpoint: 1920,
//        settings: {
//          slidesToShow: 5,
//        }
//      },
//      {
//        breakpoint: 1367,
//        settings: {
//          slidesToShow: 3,
//        }
//      }
//    ]
//   });
  

// $('#animatedHeading').slick({
//     vertical:true,
//     verticalSwiping:true,
//     slidesToShow: 5,
//     slidesToScroll: 1,
//     autoplay: true,
//     autoplaySpeed: 0,
//     speed:5000,
//     cssEase: 'linear',
//     infinite: true,
//     arrows:false,
//     touchMove:true,
//     swipeToSlide:true,
//     swipe:true,
//     responsive: [
//       {
//         breakpoint: 1920,
//         settings: {
//           slidesToShow: 5,
//         }
//       },
//       {
//         breakpoint: 1605,
//         settings: {
//           slidesToShow: 3,
//         }
//       },
//       {
//         breakpoint: 1367,
//         settings: {
//           slidesToShow: 3,
//         }
//       }
//     ]
// })

const Confettiful = function (el) {
  this.el = el;
  this.containerEl = null;

  this.confettiFrequency = 3;
  this.confettiColors = ['#EF2964', '#00C09D', '#2D87B0', '#48485E', '#EFFF1D'];
  this.confettiAnimations = ['slow', 'medium', 'fast'];

  this._setupElements();
  this._renderConfetti();
};

Confettiful.prototype._setupElements = function () {
  const containerEl = document.createElement('div');
  const elPosition = this.el.style.position;

  if (elPosition !== 'relative' || elPosition !== 'absolute') {
    this.el.style.position = 'relative';
  }

  containerEl.classList.add('confetti-container');

  this.el.appendChild(containerEl);

  this.containerEl = containerEl;
};

Confettiful.prototype._renderConfetti = function () {
  this.confettiInterval = setInterval(() => {
    const confettiEl = document.createElement('div');
    const confettiSize = Math.floor(Math.random() * 3) + 7 + 'px';
    const confettiBackground = this.confettiColors[Math.floor(Math.random() * this.confettiColors.length)];
    const confettiLeft = Math.floor(Math.random() * this.el.offsetWidth) + 'px';
    const confettiAnimation = this.confettiAnimations[Math.floor(Math.random() * this.confettiAnimations.length)];

    confettiEl.classList.add('confetti', 'confetti--animation-' + confettiAnimation);
    confettiEl.style.left = confettiLeft;
    confettiEl.style.width = confettiSize;
    confettiEl.style.height = confettiSize;
    confettiEl.style.backgroundColor = confettiBackground;

    confettiEl.removeTimeout = setTimeout(function () {
      confettiEl.parentNode.removeChild(confettiEl);
    }, 3000);

    this.containerEl.appendChild(confettiEl);
  }, 25);
};

window.confettiful = new Confettiful(document.querySelector('.js-container'));

// loaddata();
});



AOS.init({
disable: function() {
    return window.innerWidth < 800;
}


});

function loaddata(){

var DivAllDonors ="";
for (i = 0; i < 8; ++i) {

  DivAllDonors += ' <li>';
  DivAllDonors += ' <div class="flex-set">';
  DivAllDonors += ' <div class="al-conrib-left">';
  DivAllDonors += ' <p> ABC Test <span>Donated</span></p>';
  DivAllDonors += ' </div>';
  DivAllDonors += ' <div class="al-conrib-right">';
  DivAllDonors += ' <p class="rec-sp">=Scholarship Funds</p>'; DivAllDonors += '<p><span class="al-month">Monthly</span><span class="al-usd">USD 2222</span> </p> </div></div></li>'; 
  
  $("#animatedHeading").empty()
  $("#animatedHeading").append(DivAllDonors);
  
  }; 


}




  



//- Explosion
